# Infrastructure as Code with GitLab and Terraform (GKE)

**DISCLAIMER: This example uses the certificate based GitLab Group Clusters feature which is now DEPRECATED**

This repository contains sample code for creating Google Kubernetes Engine (GKE) [Group level clusters](https://docs.gitlab.com/ee/user/group/clusters/) with the [GitLab Infrastructure as Code](https://docs.gitlab.com/ee/user/infrastructure/).

For more information on how to use it, please refer to the [official docs](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html).
